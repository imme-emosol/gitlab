---
type: howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Import project from repository by URL **(FREE)**

You can import your existing repositories by providing the Git URL:

<!-- vale gitlab.Spelling = NO -->
<!-- vale gitlab.SubstitutionWarning = NO -->

1. From your GitLab dashboard select **New project**.
1. Switch to the **Import project** tab.
1. Select **Repo by URL**.
1. Fill in the "Git repository URL" and the remaining project fields.
1. Select **Create project** to begin the import process.
1. Once complete, you are redirected to your newly created project.

<!-- vale gitlab.Spelling = YES -->
<!-- vale gitlab.SubstitutionWarning = YES -->

![Import project by repository URL](img/import_projects_from_repo_url.png)

## Automate group and project import **(PREMIUM)**

For information on automating user, group, and project import API calls, see
[Automate group and project import](index.md#automate-group-and-project-import).
